namespace Domain
{
    public class CartEntry
    {
        public CartEntry(int number, string type)
        {
            this.Number = number;
            this.Type = type;
        }

        public int Number { get; set; }

        public string Type { get; set; }
    }
}