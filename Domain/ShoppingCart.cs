namespace Domain
{
    using System.Collections.Generic;
    using System.Linq;

    public class ShoppingCart
    {
        private readonly List<CartEntry> cartEntries;
        private string customer;

        public ShoppingCart()
        {
            this.cartEntries = new List<CartEntry>();
        }

        public DeliveryOption DeliveryOption { get; private set; }

        public void Add(int number, string type)
        {
            this.cartEntries.Add(new CartEntry(number, type));
        }

        public void Checkout()
        {
            var numberOfBooks = this.cartEntries
                .Where(x => x.Type == "books")
                .Sum(x => x.Number);

            this.DeliveryOption =
                this.customer == "VIP" && numberOfBooks >= 5
                ? DeliveryOption.Free
                : DeliveryOption.Standard;
        }

        public void SetCustomer(string customerType)
        {
            this.customer = customerType;
        }
    }
}