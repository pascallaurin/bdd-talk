namespace Domain
{
    public enum DeliveryOption
    {
        Unknown,
        Standard,
        Free
    }
}