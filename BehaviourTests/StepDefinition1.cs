﻿using System;

using Domain;

using TechTalk.SpecFlow;

using Xunit;

namespace BehaviourTests
{
    [Binding]
    public class StepDefinition1
    {
        [Given(@"I am a (.*) customer")]
        public void GivenIAmAXCustomer(string customerType)
        {
            var shoppingCart = new ShoppingCart();
            shoppingCart.SetCustomer(customerType);

            ScenarioContext.Current.Set(shoppingCart);
        }

        [Given(@"I have added (\d+) (.*) to the cart")]
        public void GivenIHaveAddedBooksToTheCart(int quantity, string typeProduct)
        {
            var shoppingCart = ScenarioContext.Current.Get<ShoppingCart>();
            shoppingCart.Add(quantity, typeProduct);
        }

        [When(@"I press checkout")]
        public void WhenIPressCheckout()
        {
            var shoppingCart = ScenarioContext.Current.Get<ShoppingCart>();
            shoppingCart.Checkout();
        }

        [Then(@"I should get the (.*) delivery option")]
        public void ThenIShouldGetTheXDeliveryOption(string deliveryOption)
        {
            var shoppingCart = ScenarioContext.Current.Get<ShoppingCart>();

            var option = Enum.Parse(typeof(DeliveryOption), deliveryOption);

            Assert.Equal(option, shoppingCart.DeliveryOption);
        }
    }
}
