﻿Feature: VIP program
    In order to entice existing customers to register for the VIP program
    As a marketing manager
    I want the system to offer free delivery on certain items to VIP customers

# Free delivery is offered to VIP customers once they purchase a certain number of books.
# Free delivery is not offered to regular customers or VIP customers buying anything other than books.
# Given that the minimum number of books to get free delivery is five, then we expect the following:

Scenario: Free delivery for VIP with 5 books
    Given I am a VIP customer
    And I have added 5 books to the cart
    When I press checkout
    Then I should get the Free delivery option

Scenario: Standard delivery for VIP with 4 books
    Given I am a VIP customer
    And I have added 4 books to the cart
    When I press checkout
    Then I should get the Standard delivery option

Scenario: Standard delivery for Regular customer with 10 books
    Given I am a Regular customer
    And I have added 10 books to the cart
    When I press checkout
    Then I should get the Standard delivery option

# Given that the minimum number of books to get free delivery is five, then we expect the following
Scenario Outline: Free delivery
    Given I am a <Customer type> customer
    And I have added <Cart content> to the cart
    When I press checkout
    Then I should get the <Delivery> delivery option

Examples: 
    |Customer type  |Cart content               |Delivery    |
    |VIP            |5 books                    |Free        |
    |VIP            |4 books                    |Standard    |
    |Regular        |10 books                   |Standard    |
    |VIP            |5 washing machine          |Standard    |
    |VIP            |5 books, 1 washing machine |Standard    |
